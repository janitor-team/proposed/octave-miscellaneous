octave-miscellaneous (1.3.0-2) unstable; urgency=medium

  * Team upload

  * d/control:
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Bump debhelper compatibility level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Sun, 02 Aug 2020 12:27:46 -0300

octave-miscellaneous (1.3.0-1) unstable; urgency=medium

  * Team upload

  * New upstream version 1.3.0
  * d/control:
    + Bump dependency on dh-octave to >= 0.7.1
      This allows the injection of the virtual package octave-abi-N into the
      package's list of dependencies.
  * d/copyright: Reflect upstream changes
  * d/p/deprecated-octave-config-info.patch: Drop patch (fixed upstream)
  * d/clean: Remove files generated at build time

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 09 Nov 2019 04:08:16 -0300

octave-miscellaneous (1.2.1-6) unstable; urgency=medium

  * Team upload

  * d/rules: Use override_dh_installdeb instead of _auto_install for cleanup
  * d/control: Bump Standards-Version to 4.4.1 (no changes needed)
  * d/p/deprecated-octave-config-info.patch: New patch.
    This patch allows the build against Octave 5.1.

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 05 Oct 2019 08:54:18 -0300

octave-miscellaneous (1.2.1-5) unstable; urgency=medium

  * Team upload

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

  [ Sébastien Villemot ]
  * Revert "d/rules: Use override_dh_installdeb for post-install actions"

  [ Rafael Laboissiere ]
  * d/rules: Use override_dh_installdeb for post-install actions
  * d/p/special_matrix_lauchli: Add DEP-3 header
  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:56:34 -0200

octave-miscellaneous (1.2.1-4) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:33:25 -0200

octave-miscellaneous (1.2.1-3) unstable; urgency=medium

  * Team upload.

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.

  [ Rafael Laboissiere ]
  * d/p/autoload-yes.patch: Remove patch (deprecated upstream)
  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Remove myself and Thomas Weber from Uploaders
    + Use secure URIs in the Vcs-* fields
    + Use cgit instead of gitweb in Vcs-Browser URL
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:13:54 -0200

octave-miscellaneous (1.2.1-2) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Add Rafael Laboissiere and Mike Miller to Uploaders.

  [ Rafael Laboissiere ]
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * debian/copyright: Comply with DEP-5 specification
  * Make physical_constant.py script non-executable

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 21 Sep 2014 11:53:45 +0200

octave-miscellaneous (1.2.1-1) unstable; urgency=medium

  [ Thomas Weber ]
  * Imported Upstream version 1.2.1
    - partarray.cc was removed upstream, making it no longer a problem for
      clang (closes: #749154)
  * Drop unused lintian override: hardening-no-fortify-functions
  * debian/control: Use canonical URLs in Vcs-* fields

  [ Rafael Laboissiere ]
  * Bump to Standards-Version 3.9.5, no changes needed

 -- Thomas Weber <tweber@debian.org>  Mon, 09 Jun 2014 15:43:35 +0200

octave-miscellaneous (1.2.0-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Use the octave-maintainers mailing list as upstream
    contact

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 15 May 2013 17:39:12 +0200

octave-miscellaneous (1.2.0-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.2.0
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Refresh for new upstream release
  * Use Sébastien Villemot's @debian.org email address
  * Remove obsolete DM-Upload-Allowed flag
  * Add patch autoload-yes.patch
  * Add copyright info for file lauchli.m (included in a Debian patch)
  * Add patch partcnt-test-succeeds.patch
  * Build-depends on octave-pkg-dev >= 1.0.3

  [ Sébastien Villemot ]
  * debian/control: fix versioned dependency on debhelper
  * Add lintian override for false positive on hardening (fortify)

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 17 Oct 2012 13:40:55 +0200

octave-miscellaneous (1.1.0-1) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 1.1.0
  * debian/patches/match-cell-array.patch: remove patch (applied upstream)
  * debian/patches/waitbar-rename.patch: remove patch (applied upstream)
  * debian/patches/no-flexml.patch: remove obsolete patch, flex no longer used
    (Closes: #666294)
  * debian/copyright: reflect upstream changes
  * debian/octave-miscellaneous.docs: remove, no more docs in the package
  * debian/clean: remove obsolete file
  * debian/rules: remove hack for wrong permissions in upstream tarball

  [ Rafael Laboissiere ]
  * debian/watch: Use the SourceForge redirector

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Mon, 02 Apr 2012 13:20:23 +0000

octave-miscellaneous (1.0.11-3) unstable; urgency=low

  * debian/patches/no-flexml.patch: new patch

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Tue, 20 Mar 2012 20:16:34 +0000

octave-miscellaneous (1.0.11-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * Bump the debhelper compatibility level to 9
  * debian/control: Bump standards version to 3.9.3, no changes needed

  [ Sébastien Villemot ]
  * Add Sébastien Villemot to Uploaders
  * Build-Depend on octave-pkg-dev >= 1.0.0
  * debian/copyright: update using machine-readable format 1.0
  * debian/patches/match-cell-array.patch: new patch
  * debian/rules: fix wrong +x perms in upstream tarball
  * debian/patches/waitbar-rename.patch: new patch

 -- Thomas Weber <tweber@debian.org>  Sun, 11 Mar 2012 23:14:58 +0100

octave-miscellaneous (1.0.11-1) unstable; urgency=low

  * New upstream release (Closes: #626264)
  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571851)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Bump Standards-Version to 3.9.1, no changes needed
  * Switch to dpkg-source 3.0 (quilt) format

 -- Thomas Weber <tweber@debian.org>  Thu, 12 May 2011 20:57:43 +0200

octave-miscellaneous (1.0.9-1) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/control: Build-depend on octave-pkg-dev >= 0.7.0, such that the
    package is built against octave3.2

  [ Thomas Weber ]
  * New upstream release
  * Removed patches (applied upstream):
    - autoload-num2hex
    - build-hex2num
    - cell2csv-str-find
  * Use octave-miscellaneous.docs for installation of documentation, following
    the change in octave-pkg-dev 0.7.1

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 22 Nov 2009 14:55:12 +0100

octave-miscellaneous (1.0.8-1) unstable; urgency=low

  * New upstream release
  * debian/patches/build-hex2num: Adjust patch for new upstream version
  * debian/patches/cell2csv-str-find: Fix bug related to the return type
    of the str.find() function
  * debian/control:
    + (Standards-Version): Bump to 3.8.1 (add file debian/README.source)
    + (Depends): Add ${misc:Depends}
    + (Vcs-Git, Vcs-Browser): Adjust to new Git repository
  * debian/patches/{autoload-num2hex,build-hex2num,special_matrix_lauchli}:
    Add description
  * debian/copyright: Use DEP5 URL in Format-Specification

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 24 May 2009 15:12:40 +0200

octave-miscellaneous (1.0.7-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Add header
  * debian/control: Bump build-dependency on octave-pkg-dev to >= 0.6.4,
    such that the package is built with the versioned packages directory

  [ Thomas Weber ]
  * Upload to unstable

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 05 Apr 2009 20:53:36 +0200

octave-miscellaneous (1.0.7-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * debian/rules: Use debian/clean instead of manually cleaning files
  * debian/compat, debian/control: Bump build-dependency on debhelper to
    >= 7.0.0, otherwise debian/clean is moot
  * debian/clean: New file

  [ Thomas Weber ]
  * New upstream release
  * Drop patches/waitbar_null_pointer_guard; applied upstream
  * Bumpd dependency on octave-pkg-dev to 0.6.1, to get new version in
    experimental
  * Add units to both build-depends and depends, the package needs it for unit
    tests

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 30 Nov 2008 23:39:44 +0100

octave-miscellaneous (1.0.6.4) unstable; urgency=low

  * New patch:
    + waitbar_null_pointer_guard: Check that the return values from tgetstr()
    are non-zero (closes: 485920).
  * Bump standards version to 3.8.0, no changes needed.

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Mon, 30 Jun 2008 09:25:13 +0200

octave-miscellaneous (1.0.6.4) unstable; urgency=low

  * New patches:
    + autoload-num2hex: Autoload num2hex function in hex2num.cc
    + build-hex2num: Typo in TARGETS list prevented hex2num.oct from being
      built

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 21 May 2008 19:38:38 +0200

octave-miscellaneous (1.0.6-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * New upstream version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Mon, 12 May 2008 10:29:48 +0200

octave-miscellaneous (1.0.5-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * Initial release (closes: #468509)

  [ Thomas Weber ]
  * Add lauchli.m from special-matrix package, avoiding the need for a
    package (special-matrix) for just one file.

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Fri, 04 Apr 2008 22:02:13 +0200
